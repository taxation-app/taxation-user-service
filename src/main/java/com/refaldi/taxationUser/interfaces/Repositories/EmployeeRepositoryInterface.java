package com.refaldi.taxationUser.interfaces.Repositories;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import com.refaldi.taxationUser.entities.EmployeeEntity;

public interface EmployeeRepositoryInterface {
    public void createEmployee(EmployeeEntity entity);
    public CompletableFuture<List<Map<String, Object>>> readAllEmployee(); // need to have return
    public void updateEmployee(EmployeeEntity entity);
    public void deleteEmployee(EmployeeEntity entity);
}
