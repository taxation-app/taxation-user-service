package com.refaldi.taxationUser.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.refaldi.taxationUser.Interceptors.ApiKeyInterceptor;

@Configuration
public class RestApiConfig implements WebMvcConfigurer {

    private ApiKeyInterceptor apiKeyInterceptor;

    public RestApiConfig(ApiKeyInterceptor apiKeyInterceptor) {
        this.apiKeyInterceptor = apiKeyInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(apiKeyInterceptor).addPathPatterns("/api/**"); // Apply interceptor to specific paths or patterns
    }
}
