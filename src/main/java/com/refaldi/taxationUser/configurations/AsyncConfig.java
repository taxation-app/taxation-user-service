package com.refaldi.taxationUser.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

//this class is to configure and enable async
@Configuration
@EnableAsync
public class AsyncConfig {
    // leave empty if you don't know what to do
}
