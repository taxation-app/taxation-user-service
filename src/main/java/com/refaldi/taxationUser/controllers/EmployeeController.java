package com.refaldi.taxationUser.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.refaldi.taxationUser.entities.EmployeeEntity;
import com.refaldi.taxationUser.repositories.EmployeeRepository;
import com.refaldi.taxationUser.utilities.ApiResponse;

@RestController
public class EmployeeController {

    private EmployeeRepository employeeRepository;

    //Using dependency injection
    public EmployeeController(EmployeeRepository employeeRepository){
        this.employeeRepository = employeeRepository;
    }

    @RequestMapping(value = "api/create/employee", method = { RequestMethod.POST }, headers = "Accept=application/json")
    public ResponseEntity<Map<String, Object>> createEmployee(@RequestBody EmployeeEntity payload){
        try {
            this.employeeRepository.createEmployee(payload);
            return ApiResponse.success(new HashMap<>(), "success");
        }
        catch (Exception e) {
            return ApiResponse.failed(e.getMessage(), "failed");
        }
    }

    @RequestMapping(value = "api/read/employee", method = { RequestMethod.GET }, headers = "Accept=application/json")
    public ResponseEntity<Map<String, Object>> readEmployee(){
        try {
            List<Map<String, Object>> result = this.employeeRepository.readAllEmployee().get();
            return ApiResponse.success(result, "success");
        }
        catch (Exception e) {
            return ApiResponse.failed(e.getMessage(), "failed");
        }
    }

    @RequestMapping(value = "api/read/employee/{id}", method = { RequestMethod.GET }, headers = "Accept=application/json")
    public ResponseEntity<Map<String, Object>> readEmployeeById(@PathVariable("id") Long id){
        try {
            List<Map<String, Object>> result = this.employeeRepository.readEmployeeById(id).get();
            return ApiResponse.success(result, "success");
        }
        catch (Exception e) {
            return ApiResponse.failed(e.getMessage(), "failed");
        }
    }

    @RequestMapping(value = "api/update/employee", method = { RequestMethod.POST }, headers = "Accept=application/json")
    public ResponseEntity<Map<String, Object>> updateEmployee(@RequestBody EmployeeEntity payload){
        try {
            this.employeeRepository.updateEmployee(payload);
            return ApiResponse.success(new HashMap<>(), "success");
        }
        catch (Exception e) {
            return ApiResponse.failed(e.getMessage(), "failed");
        }
    }

    @RequestMapping(value = "api/delete/employee", method = { RequestMethod.POST }, headers = "Accept=application/json")
    public ResponseEntity<Map<String, Object>> deleteEmployee(@RequestBody EmployeeEntity payload){
        try {
            this.employeeRepository.deleteEmployee(payload);
            return ApiResponse.success(new HashMap<>(), "success");
        }
        catch (Exception e) {
            return ApiResponse.failed(e.getMessage(), "failed");
        }
    }
}
