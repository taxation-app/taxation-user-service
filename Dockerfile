# Use the official OpenJDK image as a base image
FROM openjdk:21

# Set the working directory in the container
WORKDIR /app

# Copy the compiled JAR file into the container at the working directory
COPY target/taxationUser-1.0.0.jar /app

# Copy the application.properties file from resources into the container at the working directory
COPY src/main/resources/application.properties /app

# Expose the port that the Spring Boot application uses
EXPOSE 8010

# Define the command to run your application when the container starts
CMD ["java", "-jar", "taxationUser-1.0.0.jar"]