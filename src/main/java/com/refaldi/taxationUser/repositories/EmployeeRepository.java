package com.refaldi.taxationUser.repositories;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import com.refaldi.taxationUser.entities.EmployeeEntity;
import com.refaldi.taxationUser.interfaces.Repositories.EmployeeRepositoryInterface;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Repository
@Transactional
public class EmployeeRepository implements EmployeeRepositoryInterface {
    
    @PersistenceContext
    private EntityManager entityManager;

    @Async
    public void createEmployee(EmployeeEntity entity) {
        // Custom update query for existing entity with ID
        Query query = entityManager.createNativeQuery(
            "INSERT INTO employee" +
            "(name, sex, marital_status, child_number, country)" +
            "VALUES(:name, :sex, :maritalStatus, :childNumber, :country);"
        );
        query.setParameter("name", entity.getName());
        query.setParameter("sex", entity.getSex());
        query.setParameter("maritalStatus", entity.getMaritalStatus());
        query.setParameter("childNumber", entity.getChildNumber());
        query.setParameter("country", entity.getCountry());
        query.executeUpdate();
    }

    @Async
    public CompletableFuture<List<Map<String, Object>>> readEmployeeById(Long id) {
        Query query = entityManager.createNativeQuery(
            "SELECT * FROM employee where id = :id"
        );

        query.setParameter("id", id);

        List<Object[]> resultList = query.getResultList();

        List<Map<String, Object>> mappedResultList = resultList.stream().map(row -> {
            Map<String, Object> rowMap = new LinkedHashMap<>();
            rowMap.put("id", row[0]);
            rowMap.put("child_number", row[1]);
            rowMap.put("country", row[2]);
            rowMap.put("marital_status", row[3]);
            rowMap.put("name", row[4]);
            rowMap.put("sex", row[5]);
            return rowMap;
        }).collect(Collectors.toList());

        return CompletableFuture.completedFuture(mappedResultList);
    }

    @Async
    public CompletableFuture<List<Map<String, Object>>> readAllEmployee() {
        // Custom update query for existing entity with ID

        // easy way
        // Query query = entityManager.createNativeQuery(
        //     "SELECT * FROM employee", EmployeeEntity.class
        // );

        // List<EmployeeEntity> resultList = query.getResultList();

        // return resultList;

        //Hard way to demo stream using
        Query query = entityManager.createNativeQuery(
            "SELECT * FROM employee"
        );

        List<Object[]> resultList = query.getResultList();

        List<Map<String, Object>> mappedResultList = resultList.stream().map(row -> {
            Map<String, Object> rowMap = new LinkedHashMap<>();
            rowMap.put("id", row[0]);
            rowMap.put("child_number", row[1]);
            rowMap.put("country", row[2]);
            rowMap.put("marital_status", row[3]);
            rowMap.put("name", row[4]);
            rowMap.put("sex", row[5]);
            return rowMap;
        }).collect(Collectors.toList());

        return CompletableFuture.completedFuture(mappedResultList);
    }

    @Async
    public void updateEmployee(EmployeeEntity entity) {

        StringBuilder queryBuilder = new StringBuilder("UPDATE employee SET ");

        // Build the query dynamically based on provided fields
        List<String> fieldsToUpdate = new ArrayList<>();

        if (entity.getId() == null) {
            return;
        }

        if (entity.getName() != null) {
            fieldsToUpdate.add("name = :name");
        }

        if (entity.getSex() != null) {
            fieldsToUpdate.add("sex = :sex");
        }

        if (entity.getMaritalStatus() != null) {
            fieldsToUpdate.add("marital_status = :maritalStatus");
        }

        if (entity.getChildNumber() != null) {
            fieldsToUpdate.add("child_number = :childNumber");
        }

        if (entity.getCountry() != null) {
            fieldsToUpdate.add("country = :country");
        }

        if(fieldsToUpdate.isEmpty()){
            return;
        }

        // Construct the final query
        queryBuilder.append(String.join(", ", fieldsToUpdate));
        queryBuilder.append(" WHERE id = :id");

        // Execute the query
        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        // Set parameters for fields to be updated
        if (entity.getName() != null) {
            query.setParameter("name", entity.getName());
        }

        if (entity.getSex() != null) {
            query.setParameter("sex", entity.getSex());
        }

        if (entity.getMaritalStatus() != null) {
            query.setParameter("maritalStatus", entity.getMaritalStatus());
        }

        if (entity.getChildNumber() != null) {
            query.setParameter("childNumber", entity.getChildNumber());
        }

        if (entity.getCountry() != null) {
            query.setParameter("country", entity.getCountry());
        }

        // Set the ID parameter
        query.setParameter("id", entity.getId());

        // Execute the update query
        query.executeUpdate();
    }

    @Async
    public void deleteEmployee(EmployeeEntity entity) {
        // Custom delete query for existing entity with ID
        if (entity.getId() == null) {
            return; // Exit early if ID is null
        }
    
        Query query = entityManager.createNativeQuery(
            "DELETE FROM employee WHERE id = :id"
        );
    
        query.setParameter("id", entity.getId());
        query.executeUpdate();
    }
}
