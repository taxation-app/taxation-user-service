
# [Project] Taxation User Service

A taxation service to calculate tax of employee/user based on country and many factors. This is a part of microservice to manage employee data so the tax data from taxation service can be inputted

## Microservices List

1. [This] Taxation User Service
2. [Taxation Service](https://gitlab.com/taxation-app/taxation-service) : to generate tax result and manage employee tax

## Properties

* Framework 			: Spring Boot 3.2.0
* Programming Lang		: Java, SQL
* Java Environment		: JDK 21
* Project Management 	: Maven
* Containerization		: Docker
* Database				: MySQL 8.0.30

## Installation (from source)

1. Install JDK 21
2. Install Maven (Newest 2023)
3. Set environment variable both for JDK and Maven
4. Install SDKMAN
5. Install Spring Boot CLI
6. Go to project folder
7. Change application.properties (if you want to use local, pls uncomment for local)
8. Create database "taxation" (both microservice using same database, so no need to create new if it's already there)
9. Run mvn spring-boot:run

## Installation (using docker, default)

1. Install JDK 21
2. Install Maven (Newest 2023)
3. Set environment variable both for JDK and Maven
4. Install SDKMAN
5. Install Spring Boot CLI
6. Go to project folder
7. Create database "taxation" (on local, because database is not setup in docker) (both microservice using same database, so no need to create new if it's already there)
8. Run mvn clean install
9. Run "docker build -t taxation-user ."
10. Run command from "dockerrun" file

## API Documentation

Pls refer to folder "documentations/Insomnia JSON/{jsonfile here}", import and test it using Insomnia Rest, further link : [https://insomnia.rest/](https://insomnia.rest/)

both have same API JSON file, one import is enough

## Developer Note

you need to start [Taxation Service](https://gitlab.com/taxation-app/taxation-service) in order to create employee tax data and calculate tax data, you can use this app standalone
