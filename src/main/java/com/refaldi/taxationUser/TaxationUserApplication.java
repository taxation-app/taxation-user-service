package com.refaldi.taxationUser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaxationUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaxationUserApplication.class, args);
	}

}
